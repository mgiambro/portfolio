<?php
// framework/index.php
require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

$request = Request::createFromGlobals();

$input = $request->get('name', 'World');

$response = new Response(sprintf('Hello %s', htmlspecialchars($input, ENT_QUOTES, 'UTF-8')));

$response->send();
//echo var_dump($request->getUri());
//$start=strpos($request->getUri(), '/portfolio')+11;
//$route= substr($request->getUri(),$start);
//$redirectUrl = 'web/front.php/' . $route;
//echo $redirectUrl;

//$redirect = new RedirectResponse("web/front.php/$route");
//$redirect->send();

