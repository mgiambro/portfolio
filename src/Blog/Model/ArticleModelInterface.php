<?php

namespace Blog\Model;

use Blog\Domain\Article;

/**
 * Description of ArticleModelInterface
 *
 * @author maurizio
 */
interface ArticleModelInterface {
    
    public function convertFormToStorageArray($params);

    public function getById($id);

    public function getList($numRows=1000000, $order="publicationDate DESC" );

    public function insert(Array $article);

    public function update($ref, $fields, $fieldref = 'ref');

    public function delete($ref, $fieldref = 'ref');
    
    public function search($searchString);
    
}
