<?php

namespace Blog\Controller;

use Symfony\Component\HttpFoundation\Request;
use Blog\Controller\BaseController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Olson\ViewHelper;

/**
 * Description of ResourceController
 *
 * @author maurizio
 */
class ResourceController extends BaseController {

    private $config;
    private $session;
    private $request;

    function __construct()
    {
        parent::__construct();
        $this->config = $this->services->get('config');
    }

    public function getResource(Request $request, $resourceDirectory = '', $resourceName = '')
    {
        return $this->templating->render("../$resourceDirectory/$resourceName");
  //      echo $request->getRequestFormat();
    }

}
