<?php

namespace Blog\Controller;

use Symfony\Component\HttpFoundation\Request;
use Blog\Controller\BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ArticleController extends BaseController {

    private $model;
    private $config;

    function __construct()
    {
        parent::__construct();
        $this->model = $this->services->get('ArticleModel');
        $this->config = $this->services->get('config');
        //      print "In SubClass constructor\n";
    }

    public function indexAction()
    {
        $homepageNumArticles = $this->config->getSettings('project', 'homepage_num_articles');
        $data = array();
        $articles = $this->model->getList($homepageNumArticles);
        $data['articles'] = $articles;
        $data['totalRows'] = sizeOf($articles);
        $data['pageTitle'] = 'Widget News';

        return $this->templating->render('homepage.php', array('data' => $data));
    }

    public function viewArticleAction($id)
    {
        if (!isset($id)) {
            return new RedirectResponse('../home/');
        }

        $data = array();

        $article = $this->model->getById((int) $id);
        $data['article'] = $article;
        $data['pageTitle'] = $data['article']->title . " | Freelance PHP Developer";
        return $this->templating->render('viewArticle.php', array('data' => $data));
    }

    public function archiveAction()
    {
        $data = array();
        $articles = $this->model->getList();
        $data['articles'] = $articles;
        $data['totalRows'] = sizeOf($articles);
        $data['pageTitle'] = "Article Archive | Freelance PHP Developer";
        return $this->templating->render('archive.php', array('data' => $data));
    }

    public function searchAction(Request $request)
    {
        $searchString = htmlspecialchars($request->get('search_field'));
        $data = array();
        $articles = $this->model->search($searchString);
        $data['articles'] = $articles;
        $data['totalRows'] = sizeOf($articles);
        $data['pageTitle'] = "Article Archive | Freelance PHP Developer";
        return $this->templating->render('searchResults.php', array('data' => $data));
    }

}
