<?php

namespace Blog\Controller;

use Symfony\Component\HttpFoundation\Request;
use Blog\Controller\BaseController;
use Symfony\Component\HttpFoundation\Session\Session;
use Blog\Domain\Article;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of AdminController
 *
 * @author maurizio
 */
class AdminController extends BaseController {

    private $model;
    private $config;
    private $session;
    private $request;

    function __construct()
    {
        parent::__construct();
        $this->model = $this->services->get('ArticleModel');
        $this->config = $this->services->get('config');
        $this->session = new Session();
        $this->request = Request::createFromGlobals();
        $this->session->start();
        $this->checkLoginStatus();
    }

    private function checkLoginStatus()
    {
        $action = $this->getAction();
        if (!$this->isLoggedIn() && $action != 'login' && $action != 'logout') {
            $this->loginAction();
        }
    }

    private function getAction()
    {
        $pathInfo = str_replace('/', '', $this->request->getPathInfo());
        return trim($pathInfo);
    }

    private function isLoggedIn()
    {
        $loggedIn = false;
        $username = $this->session->get('username');
        if (isset($username)) {
            $loggedIn = true;
        }
        return $loggedIn;
    }

    public function loginAction()
    {
        $data = array();
        $data['pageTitle'] = "Admin Login | Widget News";
        $adminUserName = $this->config->getSettings('project', 'admin_user_name');
        $adminUserPassword = $this->config->getSettings('project', 'admin_user_password');
        $username = $this->request->get('username');
        $password = $this->request->get('password');

        if ($this->getAction() == 'login') {
            // User has posted the login form: attempt to log the user in
            if ($username == $adminUserName && $password == $adminUserPassword) {
                // Login successful: Create a session and redirect to the list articles action
                $this->session->set('username', $adminUserName);
                return $this->listArticlesAction();
            } else {
                // Login failed: display an error message to the user
                $data['errorMessage'] = "Incorrect username or password. Please try again.";
                return $this->templating->render('/admin/login.php', array('data' => $data));
            }
        } else {
            // User has not posted the login form yet: display the form
            return $this->templating->render('/admin/login.php', array('data' => $data));
        }
    }

    public function logoutAction()
    {
        $this->session->remove('username');
        return $this->templating->render('/admin/login.php', array('data' => null));
    }

    public function newArticleAction()
    {
        $data = array();
        $data['pageTitle'] = "New Article";
        $data['formAction'] = "new-article/";
        $data['session'] = $this->session;

        if ($this->request->get('saveChanges') != null) {
            // User has posted the article edit form: save the new article
            $article = $this->model->convertFormToStorageArray($this->request);
            $this->model->insert($article);
            return $this->listArticlesAction();
        } elseif ($this->request->get('cancel') != null) {
            // User has cancelled their edits: return to the article list
            return $this->listArticlesAction();
        } else {
            // User has not posted the article edit form yet: display the form
            $data['article'] = Article::create();
            return $this->templating->render('/admin/editArticle.php', array('data' => $data));
        }
    }

    public function editArticleAction($id = null)
    {
        $data = array();
        $data['pageTitle'] = "Edit Article";
        $data['formAction'] = "edit-article";
        $data['session'] = $this->session;

        if ($this->request->get('saveChanges') != null) {
            // User has posted the article edit form: save the article changes           
            $article = $this->model->getById($id);

            if (!$article) {
                return new RedirectResponse('../list-articles?error=articleNotFound');
            }
            $articleArray = $this->model->convertFormToStorageArray($this->request);

            $this->model->update($id, $articleArray, 'id');
            //        return $this->templating->render('/admin/listArticles.php', array('data' => $data));
            return new RedirectResponse('../list-articles?status=changesSaved');
        } elseif ($this->request->get('cancel') != null) {
            // User has cancelled their edits: return to the article list
            return new RedirectResponse('../list-articles');
        } else {
            // User has not posted the article edit form yet: display the form
            //               echo $id; exit;
            $data['article'] = $this->model->getById($id);
            //       echo var_dump(    date( 'j M Y',$data['article']->publicationDate)); exit;
            return $this->templating->render('/admin/editArticle.php', array('data' => $data));
        }
    }

    public function deleteArticleAction($id)
    {
        $data = array();
        $article = $this->model->getById($id);

        if (!$article) {
            return new RedirectResponse('../list-articles?error=articleNotFound');
        }
        $data['statusMessage'] = 'Article deleted.';
        $this->model->delete($article->getId());
        return new RedirectResponse('../list-articles?status=articleDeleted');
    }

    public function listArticlesAction()
    {
        $data = array();
        $articles = $this->model->getList();
        $data['articles'] = $articles;
        $data['totalRows'] = sizeOf($articles);
        $data['pageTitle'] = "All Articles";
        $data['session'] = $this->session;

        $error = $this->request->get('error');
        if (isset($error)) {
            if ($error == 'articleNotFound') {
                $data['errorMessage'] = "Error: Article not found.";
            }
        }
        $status = $this->request->get('status');

        if (isset($status)) {
            if ($status == 'changesSaved') {
                $data['statusMessage'] = 'Your changes have been saved';
            }
            if ($status == 'articleDeleted') {
                $data['statusMessage'] = 'Article Deleted';
            }
        }

        return $this->templating->render('/admin/listArticles.php', array('data' => $data));
    }

}
