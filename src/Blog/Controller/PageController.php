<?php

use Blog\Controller\BaseController;

namespace Blog\Controller;

/**
 * Description of PageController
 *
 * @author maurizio
 */
class PageController extends BaseController {

    public function indexAction($pageName = 'homepage')
    {
        $data = array();
        return $this->templating->render('pages/' . $pageName . '.php', array('data' => $data));
    }

}
