<?php

// example.com/src/app.php
use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();

// Article
$routes->add('/', new Routing\Route('/home/', array(
    '_controller' => 'Blog\\Controller\\ArticleController::indexAction',
)));

$routes->add('home', new Routing\Route('/home/', array(
    '_controller' => 'Blog\\Controller\\ArticleController::indexAction',
)));

$routes->add('view-article', new Routing\Route('/view-article/{id}', array(
    'id' => null,
    '_controller' => 'Blog\\Controller\\ArticleController::viewArticleAction',
)));

$routes->add('archive', new Routing\Route('/archive/', array(
    '_controller' => 'Blog\\Controller\\ArticleController::archiveAction',
)));

$routes->add('search', new Routing\Route('/search/', array(
    '_controller' => 'Blog\\Controller\\ArticleController::searchAction',
)));

// Admin
$routes->add('login', new Routing\Route('/login/', array(
    '_controller' => 'Blog\\Controller\\AdminController::loginAction',
)));

$routes->add('logout', new Routing\Route('/logout/', array(
    '_controller' => 'Blog\\Controller\\AdminController::logoutAction',
)));

$routes->add('admin', new Routing\Route('/admin/', array(
    '_controller' => 'Blog\\Controller\\AdminController::loginAction',
)));

$routes->add('edit-article', new Routing\Route('/edit-article/{id}', array(
    'id' => 1,
    '_controller' => 'Blog\\Controller\\AdminController::editArticleAction',
)));

$routes->add('new-article', new Routing\Route('/new-article/', array(
    '_controller' => 'Blog\\Controller\\AdminController::newArticleAction',
)));

$routes->add('delete-article', new Routing\Route('/delete-article/{id}', array(
    'id' => null,
    '_controller' => 'Blog\\Controller\\AdminController::deleteArticleAction',
)));

$routes->add('list-articles', new Routing\Route('/list-articles/{message}', array(
    'message' => null,
    '_controller' => 'Blog\\Controller\\AdminController::listArticlesAction',
)));

$routes->add('pages', new Routing\Route('/pages/{pageName}', array(
    'message' => null,
    '_controller' => 'Blog\\Controller\\PageController::indexAction',
)));

$routes->add('mail', new Routing\Route('/mail/', array(
    'message' => null,
    '_controller' => 'Blog\\Controller\\MailController::indexAction',
)));

$routes->add('resources', new Routing\Route('/resources/{resourceDirectory}/{resourceName}', array(
    '_controller' => 'Blog\\Controller\\ResourceController::getResource',
)));

/*
$routes->add('test', new Routing\Route('/test/', array(
    '_controller' => 'Test\\Controller\\TestController::indexAction',
)));


$routes->add('leap_year', new Routing\Route('/is_leap_year/{year}', array(
    'year' => null,
    '_controller' => 'Calendar\\Controller\\LeapYearController::indexAction',
)));
*/


return $routes;
