<?php include __DIR__ . "../../include/header.php" ?>
<?php
use Olson\ViewHelper;
?>
<section id="intro">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 text-center">
                <div class="intro animate-box">
                    <h2>Career History</h2>
                </div>
                <div class="intro animate-box">
                    I have <strong>over 16 years experience</strong> working with a wide variety of technologies including PHP, Java and Web Front End technologies in various types of businesses including Financial and Media companies. I have worked on and developed a wide variety of applications including  Customer Relationship Management (CRM), HR Holiday and Business Travel Booking, Personalised Support, eCommerce and Bulk Email Sending systems. To get an idea of my work, please feel free to download my <a href="<?php echo ViewHelper::getProjectRoot() . 'web/front.php/pages/code' ?>">example projects</a>. These are not overly-complex projects, rather demo projects to provide some code examples.
                </div>
        </div>
        <div>
</section>
 <?php include __DIR__ . "../../include/footer.php" ?>

