<?php include __DIR__ . "../../include/header.php";
use Olson\ViewHelper;
?>
<div id="site_content">
    <div id="sidebar_container">
        <div class="sidebar">
            <div class="sidebar_top"></div>
            <div class="sidebar_item">
                <!-- insert your sidebar items here -->
                <h4>Download CV:&nbsp<a href="<?php echo ViewHelper::getResourcesPath() . '/files/CV (Maurizio Giambrone).docx' ?>"> <img src="<?php echo ViewHelper::getResourcesPath() . '/css/icon_word_small.png'?>" alt="CV"></a></h4>
            </div>
            <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
            <div class="sidebar_top"></div>
            <?php include __DIR__ . "../../include/links.php" ?>
            <div class="sidebar_base"></div>
        </div>
    </div>

    <div id="content">
        <!-- insert the page content here -->
        <h2>Career Summary</h2>
                    I have over 16 years experience working with a wide variety of technologies in various types of businesses including Financial and Media companies. I have worked on and developed a wide variety of applications including  Customer Relationship Management (CRM), HR Holiday and Business Travel Booking, Personalised Support, eCommerce and Bulk Email Sending systems. To get an idea of my work, please feel free to download my <a href="<?php echo ViewHelper::getProjectRoot() . 'web/front.php/pages/code' ?>">example projects</a>. These are not overly-complex projects, rather demo projects to illustrate how I code.
        <h2>Skills Summary</h2>
        <ul>
            <li>PHP (incl. Laravel, Symfony Components, PHPUnit, Behat, PDO)</li>
            <li>Java (incl. Spring MVC, Struts 2, Hibernate, Junit, Jsp, Ant, Maven)</li>
            <li>HTML, JavaScript, CSS, JQuery</li>
            <li>MySQL, Microsoft SQL Server</li>
            <li>Amazon Web Services, Linux</li>
            <li>Git, Subversion, Cruise Control, Jenkins</li>
            <li>Lotus Notes / Domino</li>
        </ul>
        <h2>Employment History</h2>
        <h3>RLA</h3>
        <h4>PHP Web Developer</h4>
        <h5>September 2015 – December 2015</h5>
        <ul>
            <li>Contributed to the development of the RLA Ad Buyer application. Ad Buyer is a template application that can be customised for different automotive clients and handles the creation of marketing materials such as pdf fliers. Ad buyer also integrates with third party email platforms such as Pure360 and Campaign Monitor to perform marketing mailouts. Ad Buyer is a PHP web application based on the Laravel framework and behaviour-driven development using behat.</li>
            <li>Provided support and development to existing Laravel websites for high profile clients such as BMW and Volkswagen.</li>
        </ul>

        <h3>Clevertech Solutions</h3>
        <h4>PHP Engineer</h4>
        <h5>April 2014 – July 2015</h5>
        <ul>
            <li>Contributed towards the development, deployment and support of an eCommerce application for a major high street bank.</li>
            <li>Responsible for administering and maintaining applications on Ubuntu Linux servers hosted on the Aws Cloud platform.</li>
            <li>Developed a Cloud Security Solution to handle the external storage, retrieval and application of encryption keys and encryption throughout the applications.</li>
            <li>Developed a bulk email application to handle email marketing and statistics, written using PHP, MySQL and Amazon SES.</li>
            <li>Developed monitoring software to monitor applications and servers within the AWS environment.</li>
        </ul>

        <h3>SmartFocus</h3>
        <h4>Java Engineer</h4>
        <h5>January 2013 – February 2014</h5>
        <ul>
            <li>Used agile test-driven development principles to contribute to the SmartFocus SAAS Personalisation Engine.</li>
            <li>Coding to provide and consume REST API web services.</li>
            <li>Coding a migration application to migrate gigabytes of data from a MySQL database to an Hbase data store.</li>
            <li>Contributed to the development of the 'Bridging' application, which provided personalisation to emails in the SmartFocus email marketing application.</li>
        </ul>

        <h3>LV=</h3>
        <h4>Ecommerce Analyst Developer</h4>
        <h5>March 2012 – December 2012</h5>
        <ul>
            <li>Analyse business requirements with regard to the 'Quote and Buy' application.</li>
            <li>Using Java, XML, Jsp, Css and HTML, made changes and enhancements to the Quote and Buy application.</li>
        </ul>

        <h3>Donovan Data Systems</h3>
        <h4>Lotus Notes / Java Developer</h4>
        <h5>February 2006 – March 2012</h5>
        <ul>
            <li>Developed a Service-Oriented Web Reporting application using Java, Spring MVC, Hibernate and RabbitMQ asynchronous Messaging.</li>
            <li>Translated business requirements, documented and developed an MVC online support application which served up personalised support documentation for users. </li>
            <li>Provided development and support for a PHP web reporting application.</li>
            <liContributed to the support and development of the in-house Lotus Notes applications.</li>
        </ul>
        <h2>Courses / Qualifications</h2>
        <ul>
            <li>Architecting on AWS</li>
            <li>VMWare SpringSource Core Spring, Spring Web and Spring Integration</li>
            <li>Introduction into Spring and Hibernate</li>
            <li>ISEB Foundation Certificate in IS Project Management</li>
            <li>Programming a Microsoft SQL Server 2000 Database </li>
            <li>The Chartered Institute of Insurance – Lloyd’s Introductory Test 
                Business Objects 6.5 Web Intelligence Reporting</li>
            <li>IBM Certified Advanced Application Developer  - Lotus Notes and Domino 6/6.5</li>
        </ul>
        <h2>Education</h2>
        <p>
            <strong>1996 – 1999 Bournemouth University</strong><br/>
            Degree: BSc Hons Applied Psychology and Computing. 2:1 Attained
        </p>

        <p>
            <strong>1993 – 1994 Bournemouth & Poole College of Further Education</strong><br/>
            RSA Advanced Diploma in Business Procedures. Merit-Distinction Attained
        </p>
    </div>
    <span id="page" style="display:none">cv</span>
    <?php include __DIR__ . "../../include/footer.php" ?>

