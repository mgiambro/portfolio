<?php include __DIR__ . "../../include/header.php" ?>
<?php
use Olson\ViewHelper;
?>
<section id="intro">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 text-center">
						<div class="intro animate-box">
							<h1>Contact Me</h1>
							<h2>If you'd like to get in touch, please use the form below:</h2>
						</div>
					</div>
				</div>
			<div>
		</section>

		<main id="main">
			<div class="container">
				<div class="col-md-8 col-md-offset-2 animate-box">
					  <form action="<?php echo \Olson\ViewHelper::getProjectRoot() . 'web/front.php/mail/' ?>" method="post">
						<div class="form-group row">
							<div class="col-md-12 field">
								<label for="name">Name</label>
								<input type="text" name="name" id="name" class="form-control">
							</div>
						
						</div>
						<div class="form-group row">
							<div class="col-md-12 field">
								<label for="email">Email</label>
								<input type="text" name="email" id="email" class="form-control">
							</div>
						
						</div>
						<div class="form-group row">
							<div class="col-md-12 field">
								<label for="message">Message</label>
								<textarea name="enquiry" id="message" cols="30" rows="10" class="form-control"></textarea>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 field">
								<input type="submit" id="submit" class="btn btn-primary" value="Send Message">
							</div>
						</div>
                                            
                                             <p class="antispam"><span>Leave this empty</span><input type="text" name="url" /></p>
					</form>
				</div>
				<!-- <div class="col-md-4"></div> -->
			</div>
		</main>
  
    
    <span id="page" style="display:none">contact</span>
    <?php include __DIR__ . "../../include/footer.php" ?>