<?php
include __DIR__ . "../../include/header.php";

use Olson\ViewHelper;
?>
<section id="intro">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 text-center">
                <div classs="intro animate-box">
                    <h2>Some example projects I have built.</h2>
                </div>
            </div>
        </div>
        <div>
            </section>

            <section id="work">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h1>Olson MVC</h1>
                            <p>
                                Olson MVC is a lightweight MVC framework built by myself. It combines tried and tested third-party components with hand-coded classes to provide a light, fast and elegant MVC framework with which to base your web application. My philosohpy is to minimalise bloat. Therefore, this framework aims to comprise the main base components necessary for a web application framework. If additional functionality is required, the developer can either choose a component from <a href="https://packagist.org/" target=_blank>packagist.org</a> or develop it themselves.
                            </p>
                            <p>Features include:</p>
                            <ul>
                                <li>Symfony Http Request and Routing components</li>
                                <li>Symfony Dependency Injection Container</li>
                                <li>Lightweight, hand-coded application configuration settings components</li>
                                <li>Lightweight, hand-coded data access layer wrapping PDO</li>
                                <li>Logging</li>
                                <li>Suggested application code structure</li>
                            </ul>
                            <ul>
                                <li>Git repository URL: https://mgiambro@bitbucket.org/mgiambro/olsonmvc.git</li>
                                <li>Direct Download: <a href="<?php echo ViewHelper::getResourcesPath() . '/files/OlsonMVC.tar.gz' ?>">Olson MVC</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h1>A Simple CMS</h1>
                            <p>A very simple CMS built on Olson MVC. It provides a simple means of managing articles via an admin section and storing them in a MySQL database. Goby CMS comes with a basic CSS theme, but can easily be styled to suit your requirements.</p>
                            <p>This is essentially intended as a demo project to give interested parties an example of my coding. However, it also serves as a good starting point for developers to use when developing their own custom CMS.</p>
                            <ul>
                                <li>Git repository URL: https://mgiambro@bitbucket.org/mgiambro/cms.git</li>
                                <li>Direct Download: <a href="<?php echo ViewHelper::getResourcesPath() . '/files/cms.tar.gz' ?>">CMS</a></li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        </section>

                        <span id="page" style="display:none">code</span>
                        <?php include __DIR__ . "../../include/footer.php" ?>