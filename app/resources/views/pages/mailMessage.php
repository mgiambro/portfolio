<?php include __DIR__ . "../../include/header.php" ?>
    
    <section id="intro">
    <div class="container">
        <div class="row">
               <?php
        if (array_key_exists('validationMessage', $data)) {
            echo '<p>' . $data['validationMessage'] . '</p>';
        } else {
            echo "<h1>Thank you for your message</h1>";
            echo "<p>I'll get back to you as soon as possible.</p>";
        }
        ?>
        </div>
        <div>
</section>
<?php include __DIR__ . "../../include/footer.php" ?>