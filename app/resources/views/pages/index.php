<?php include __DIR__ . "../../include/header.php" ?>
<?php
use Olson\ViewHelper;
?>
<section id="intro">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 text-center">
                <div class="intro animate-box">
                    <h2>Welcome to my personal PHP Web Development site.</h2>
                </div>
                 <div class="intro animate-box">
                    Hello! I'm Maurizio Giambrone, a PHP Web Developer based in Bournemouth, Dorset.</p>
                </div>
            </div>
        </div>
        <div>
</section>
 <?php include __DIR__ . "../../include/footer.php" ?>
