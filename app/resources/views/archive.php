<?php include "include/header.php" ?>
 
      <h1>Article Archive</h1>
 
      <ul id="headlines" class="archive">
 
<?php foreach ( $data['articles'] as $article ) { ?>
 
        <li>
          <h2>
            <span class="pubDate"><?php echo date('j F Y', $article->publicationDate)?></span><a href="../view-article/<?php echo $article->id?>"><?php echo htmlspecialchars( $article->title )?></a>
          </h2>
          <p class="summary"><?php echo htmlspecialchars( $article->summary )?></p>
        </li>
 
<?php } ?>
 
      </ul>
 
      <p><?php echo $data['totalRows']?> article<?php echo ( $data['totalRows'] != 1 ) ? 's' : '' ?> in total.</p>
 
      <p><a href="../home/">Return to Homepage</a></p>
 
<?php include "include/footer.php" ?>
