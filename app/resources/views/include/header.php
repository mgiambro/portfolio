<?php
use Olson\ViewHelper;
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PHP Developer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="description" content="Personal website of software developer Maurizio Giambrone" />
                <meta name="keywords" content="freelance, freelancer, independent, php, object oriented, web developer, web development, bespoke, symfony, laravel, java, bournemouth, dorset, uk" />
        <meta name="author" content="FREEHTML5.CO" />

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!-- <link rel="shortcut icon" href="favicon.ico"> -->

        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700|Roboto:300,400' rel='stylesheet' type='text/css'>

        <!-- Animate.css -->
        <link rel='stylesheet' type='text/css' href="/assets/css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link  rel='stylesheet' type='text/css' href="/assets/css/icomoon.css">
        <!-- Bootstrap  -->
        <!-- <link rel="stylesheet" href="{{ asset(bootstrap.css) }}" > -->
        <link rel='stylesheet' type='text/css' href="/assets/css/bootstrap.css">
        <link rel='stylesheet' type='text/css' href="/assets/css/style.css">

        <!-- Modernizr JS -->
        <script src="/assets/js/modernizr-2.6.2.min.js"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
                  <!-- <script
                    src="https://code.jquery.com/jquery-2.2.3.min.js"
                    integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="
                crossorigin="anonymous"></script> -->
    </head>
  	<body>
	<div class="box-wrap">
		<header role="banner" id="fh5co-header">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="row">
						<div class="col-md-3">
							<div class="fh5co-navbar-brand">
                  <a class="fh5co-logo" href="index.html"><img src="<?php echo ViewHelper::getProjectRoot() . 'resources/images/brand-nav.png' ?>" alt="Closest Logo"></a>
							</div>
						</div>
						<div class="col-md-6">
							<ul class="nav text-center">
								<li class="active"><a href="<?php echo ViewHelper::getProjectRoot() . 'pages/index' ?>"><span>Home</span></a></li>
								<li><a href="<?php echo ViewHelper::getProjectRoot() . 'pages/about' ?>">About</a></li>
								<li><a href="<?php echo ViewHelper::getProjectRoot() . 'pages/code' ?>">Projects</a></li>
								<li><a href="<?php echo ViewHelper::getProjectRoot() . 'pages/contact' ?>">Contact</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<ul class="social">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-dribbble"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</nav>
		  </div>
		</header>
		<!-- END: header -->
