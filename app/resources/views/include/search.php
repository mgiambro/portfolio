<?php

use Olson\ViewHelper;

?>
<div class="sidebar_item">
    <h3>Search</h3>
    <form method="post" action="<?php echo ViewHelper::getProjectRoot() . 'web/front.php/search/' ?>" id="search_form">
        <p>
            <input class="search" type="text" name="search_field" onclick="javascript:this.value=''" value="Enter keywords....." />
            <input name="search" type="image" onclick="javascript:this.form.submit;" style="border: 0; margin: 0 0 -9px 5px;" src="<?php echo ViewHelper::getResourcesPath() . '/css/search.png' ?>" alt="Search" title="Search" />
        </p>
    </form>
</div>

