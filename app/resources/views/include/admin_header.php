<!DOCTYPE html>
<?php

use Olson\ViewHelper;

?>
<html lang="en">
    <head>
        <title><?php echo htmlspecialchars($data['pageTitle']) ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::getResourcesPath() . '/css/style_admin.css' ?>" />
    </head>
    <body>
        <div id="container">

            <a href="."><img id="logo" src="<?php echo ViewHelper::getResourcesPath() . '/images/logo.jpg' ?>" alt="Goby CMS" /></a>

