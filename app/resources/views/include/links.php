<div class="sidebar_item">
    <ul>
        <li><a href="http://www.phptherightway.com/" target=_blank>PHP The Right Way</a></li>
        <li><a href="http://blog.stuartherbert.com/php/php-components/" target=_blank>Beyond Frameworks - PHP Components</a></li>
        <li><a href="http://blog.cleancoder.com/" target=_blank>Clean Coder Blog</a></li>
        <li><a href="https://packagist.org/" target=_blank>Packagist - The PHP Package Repository</a></li>
        <li><a href="https://www.webbedfeet.uk/news/10-reasons-why-web-developers-should-not-use-wordpress" target=_blank>10 Reasons to avoid Wordpress</a></li>
    </ul>
</div>