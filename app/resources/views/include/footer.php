<?php
use Olson\ViewHelper;
?>

<footer id="footer" role="contentinfo">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center ">
						<div class="footer-widget border">
							<p class="pull-left"><small>Copyright &copy; Maurizio Giambrone 2017</small></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<!-- END: box-wrap -->

	<!-- jQuery -->
  <script src="/assets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
  <script src="/assets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
  <script src="/assets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
  <script src="/assets/js/jquery.waypoints.min.js"></script>

	<!-- Main JS (Do not remove) -->
  <script src="/assets/js/main.js"></script>

	</body>
</html>
