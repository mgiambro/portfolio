<?php include "include/header.php" ?>
<div id="site_content">
    <div id="sidebar_container">
        <div class="sidebar">
            <div class="sidebar_top"></div>
            <?php include "include/news.php" ?>  
            <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
            <div class="sidebar_top"></div>
            <?php include "include/links.php" ?>
            <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
            <div class="sidebar_top"></div>
            <?php include "include/search.php" ?>  
            <div class="sidebar_base"></div>
        </div>
    </div>
    <div id="content">
        <!-- insert the page content here -->
        <h1>A Page</h1>

        <?php foreach ($data['articles'] as $article) { ?>

            <li>
                <h2>
                    <span class="pubDate"><?php echo date('j F', $article->publicationDate) ?></span><a href="../view-article/<?php echo $article->id ?>"<?php echo htmlspecialchars($article->title) ?></a>
                </h2>
                <p class="summary"><?php echo htmlspecialchars($article->summary) ?></p>
            </li>

        <?php } ?>
    </div>
</div>

<?php include "include/footer.php" ?>

