<?php

namespace Config;

/**
 * Description of Config
 *
 * @author maurizio
 */
class Config {

    private $settingsArray = array();

    public function __contruct()
    {
        $pathinfo = pathinfo(__DIR__);
        $path = $pathinfo['dirname'] . '/settings/settings.php';
        include $path;

        $this->settingsArray = $global_settings;
    }

    public function getSettings($section = '', $item = '')
    {
        if ($item == '') {
            return $this->settingsArray[$section];
        } else {
            return $this->settingsArray[$section][$item];
        }
    }

}
